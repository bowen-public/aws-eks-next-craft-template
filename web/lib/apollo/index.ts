import { ApolloClient, HttpLink, InMemoryCache } from "@apollo/client";
import { nextApollo } from "@bowendev/apollo";
import getConfig from "next/config";
import qs from "querystring";

const { serverRuntimeConfig } = getConfig();
const { isBuildTime } = serverRuntimeConfig;

let apiUrl = process.env.NEXT_PUBLIC_MAIN_SITE_URL + "/api/graphql";

if (isBuildTime) {
  // building while in gitlab ci or locally
  apiUrl = "https://" + process.env.CRAFT_PRODUCTION_HOST + "/graphql";
}

export const { initializeApollo, useApollo } = nextApollo<{ token?: string }>(
  (options) => {
    const { token } = options || {};
    const mainLink = new HttpLink({
      uri: apiUrl + tokenQueryStr(token)
    });
    return new ApolloClient({
      ssrMode: typeof window === "undefined",
      link: mainLink,
      cache: new InMemoryCache()
    });
  }
);

const tokenQueryStr = (token: string | undefined): string => {
  if (token) {
    return "?" + qs.stringify({ token });
  }
  return "";
};
