module.exports = {
  images: {
    domains: ["$CI_PROJECT_.nyc3.cdn.digitaloceanspaces.com"],
    deviceSizes: [375, 768, 1440, 1920]
  }
};
