import { FunctionComponent } from "react";
import styles from "./Header.module.scss";

const Header: FunctionComponent = () => {
  return <header className={styles.header}></header>;
};

export default Header;
