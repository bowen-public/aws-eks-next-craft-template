import { cn } from "@bowendev/cn";
import { FunctionComponent, ReactNode } from "react";
import styles from "./Container.module.scss";

const Container: FunctionComponent<{
  className?: string;
  children?: ReactNode;
}> = (props) => {
  const { children, className } = props;

  return <div className={cn(styles.container, className)}>{children}</div>;
};

export default Container;
