import useScrollbarWidth from "hooks/useScrollbarWidth";
import { FunctionComponent, ReactNode } from "react";
import { SeoMarkup } from "types";
import Footer from "./Footer";
import Header from "./Header";
import HeadTag from "./HeadTag";
import styles from "./Layout.module.scss";

const Layout: FunctionComponent<{
  children?: ReactNode;
  seoMarkup?: SeoMarkup;
}> = ({ children, seoMarkup }) => {
  const sbw = useScrollbarWidth();

  return (
    <>
      {typeof window !== "undefined" && (
        <style jsx global>
          {`
            :root {
              --scrollWidth: ${sbw}px;
            }
          `}
        </style>
      )}
      <HeadTag seoMarkup={seoMarkup} />
      <div className={styles.app}>
        <Header />
        <main className={styles.main}>{children}</main>
        <Footer />
      </div>
    </>
  );
};

export default Layout;
