import { FunctionComponent } from "react";
import styles from "./Footer.module.scss";

const Footer: FunctionComponent = () => {
  return <footer className={styles.footer}></footer>;
};

export default Footer;
