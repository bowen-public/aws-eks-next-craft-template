import Head from "next/head";
import { FunctionComponent } from "react";
import parse from "react-html-parser";
import { SeoMarkup } from "types";

const HeadTag: FunctionComponent<{ seoMarkup?: SeoMarkup }> = ({
  seoMarkup
}) => {
  const seoBlob = seoMarkup
    ? [
        seoMarkup.metaTitleContainer,
        seoMarkup.metaTagContainer,
        seoMarkup.metaLinkContainer
      ].join("")
    : "";

  return (
    <>
      <Head>
        {parse(seoBlob)}
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"
        />
      </Head>
    </>
  );
};

export default HeadTag;
