import Container from "components/Container";
import { FunctionComponent } from "react";

const HomepageMain: FunctionComponent = () => {
  return <Container></Container>;
};

export default HomepageMain;
