import { gql, useQuery } from "@apollo/client";
import { SeoMarkup } from "types";

export const HOMEPAGE = gql`
  {
    homepage: entry(section: "homepage") {
      id
      uri
      title
      seomatic {
        metaTitleContainer
        metaTagContainer
        metaLinkContainer
      }
    }
  }
`;

export interface Homepage {
  homepage: {
    id: string;
    uri: string;
    title: string;
    seomatic: SeoMarkup;
  };
}

export const useHomepageData = (): Homepage => {
  const { data } = useQuery<Homepage>(HOMEPAGE);

  // asserting Homepage because we know it will not be undefined since it is prebuilt in ISG
  return data as Homepage;
};
