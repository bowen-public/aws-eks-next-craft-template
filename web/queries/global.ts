import { gql, useQuery } from "@apollo/client";

export const GLOBAL = gql`
  {
    companyInfo: globalSet(handle: "companyInfo") {
      id
      ... on companyInfo_GlobalSet {
        instagram
        facebook
        twitter
        linkedin
      }
    }
  }
`;

export interface Global {
  companyInfo: {
    id: string;
    instagram: string | null;
    facebook: string | null;
    twitter: string | null;
    linkedin: string | null;
  };
}

export const useGlobalData = (): Global => {
  const { data } = useQuery<Global>(GLOBAL);

  // asserting Global because we know it will not be undefined since it is prebuilt in ISG
  return data as Global;
};
