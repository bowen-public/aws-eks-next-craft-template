import styles from "scss/vars.module.scss";
import useMedia from "use-media";

interface Breakpoints {
  isLtWide: boolean;
  isLtDesktop: boolean;
  isLtLaptop: boolean;
  isLtTablet: boolean;
  isGtDesktop: boolean;
  isGtLaptop: boolean;
  isGtTablet: boolean;
  isGtPhone: boolean;
  isEqDesktop: boolean;
  isEqLaptop: boolean;
  isEqTablet: boolean;
}

const useBreakpoints = (): Breakpoints => {
  const isLtWide = useMedia({ maxWidth: styles.wideBreak });
  const isLtDesktop = useMedia({ maxWidth: styles.desktopBreak });
  const isLtLaptop = useMedia({ maxWidth: styles.laptopBreak });
  const isLtTablet = useMedia({ maxWidth: styles.tabletBreak });
  const isGtDesktop = useMedia({ minWidth: parseAdd1(styles.wideBreak) });
  const isGtLaptop = useMedia({ minWidth: parseAdd1(styles.desktopBreak) });
  const isGtTablet = useMedia({ minWidth: parseAdd1(styles.laptopBreak) });
  const isGtPhone = useMedia({ minWidth: parseAdd1(styles.tabletBreak) });

  return {
    isLtWide,
    isLtDesktop,
    isLtLaptop,
    isLtTablet,
    isGtDesktop,
    isGtLaptop,
    isGtTablet,
    isGtPhone,
    isEqDesktop: isLtWide && isGtLaptop,
    isEqLaptop: isLtDesktop && isGtTablet,
    isEqTablet: isLtLaptop && isGtPhone
  };
};

export default useBreakpoints;

const parseAdd1 = (s: string): string => {
  return (parseInt(s) + 1).toString() + "px";
};
