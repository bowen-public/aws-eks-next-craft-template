export interface SeoMarkup {
  metaTitleContainer: string;
  metaTagContainer: string;
  metaLinkContainer: string;
}
