import Layout from "components/Layout";
import { FunctionComponent } from "react";

const Homepage: FunctionComponent = () => {
  return <Layout>home</Layout>;
};

export default Homepage;
