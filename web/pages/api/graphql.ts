import { NextApiRequest, NextApiResponse } from "next";
import qs from "querystring";

const headers: Record<string, string> = {
  "Content-Type": "application/json"
};

if (process.env.CRAFT_GRAPHQL_AUTH_TOKEN) {
  headers.Authorization = `Bearer ${process.env.CRAFT_GRAPHQL_AUTH_TOKEN}`;
}

export default async function (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> {
  const token = getToken(req);
  const craftRes = await getCraftResponse(req, token);

  res.status(200).json(craftRes);
}

const getCraftResponse = async (
  req: NextApiRequest,
  token: string | undefined
): Promise<Response> => {
  // console.log("from graphql regular", req.body);
  const fetchRes = await fetch(
    process.env.NEXT_PUBLIC_API_URL + "/graphql" + getQueryString(token),
    {
      method: "POST",
      body: JSON.stringify(req.body),
      headers
    }
  );
  return await fetchRes.json();
};

const getQueryString = (token: string | undefined): string => {
  if (typeof token === "string") {
    return "?" + qs.stringify({ token });
  }
  return "";
};

const getToken = (req: NextApiRequest): string | undefined => {
  if (typeof req.query.token === "string") {
    return req.query.token;
  }
  return undefined;
};
