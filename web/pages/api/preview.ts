import { NextApiRequest, NextApiResponse } from "next";

export default async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  const { query } = req;
  const url = process.env.NEXT_PUBLIC_MAIN_SITE_URL;
  const uri = query.uri === "__home__" ? "" : query.uri;
  const path = "/" + uri;
  const fullUrl = url + path;

  if (query.secret !== process.env.CRAFT_PREVIEW_SECRET || !query.uri) {
    return res.status(401).json({ message: "Invalid token" });
  }

  // set cookies on response in order to get them below
  // these never reach craft because of the iframe, but we don't care
  // passing in the token to be able to receive it inside getStaticProps or getServerSideProps
  res.setPreviewData({ token: query.token });
  // get the headers that we just set above
  const headers = res.getHeaders();
  const cookiesToSet = headers["set-cookie"] as string[];
  const Cookie = cookiesToSet.join(";");

  // fetch with those set headers from server side right here
  const previewRes = await fetch(fullUrl, {
    method: "GET",
    headers: { Cookie }
  });
  const htmlText = await previewRes.text();

  // send back the response as plain text for the iframe
  res.end(htmlText);
};
