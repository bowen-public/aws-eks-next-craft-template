import { ApolloProvider } from "@apollo/client";
import "intersection-observer";
import { useApollo } from "lib/apollo";
import { AppProps } from "next/app";
import Router from "next/router";
import "node_modules/nprogress/nprogress.css";
import NProgress from "nprogress";
import { FunctionComponent, useEffect } from "react";
import TagManager from "react-gtm-module";
import "styles/index.scss";

Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

const gtmId = process.env.GTM_ID;

const CustomApp: FunctionComponent<AppProps> = (appProps) => {
  const { Component, pageProps } = appProps;
  const apolloClient = useApollo(pageProps);

  useEffect(() => {
    if (gtmId) {
      TagManager.initialize({ gtmId });
    }
  }, []);

  return (
    <ApolloProvider client={apolloClient}>
      {/* <GlobalContextProvider> */}
      <Component {...pageProps} />
      {/* </GlobalContextProvider> */}
    </ApolloProvider>
  );
};

export default CustomApp;
