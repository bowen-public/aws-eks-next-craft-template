const baseDomain = "";
const nextProductionHost = process.env.NEXT_PRODUCTION_HOST;
const nextStagingHost = process.env.NEXT_STAGING_HOST;
const craftProductionHost = process.env.CRAFT_PRODUCTION_HOST;
const craftStagingHost = process.env.CRAFT_STAGING_HOST;

export default () => {
  const common = {
    NEXT_PRODUCTION_HOST: nextProductionHost,
    NEXT_STAGING_HOST: nextStagingHost,
    CRAFT_PRODUCTION_HOST: craftProductionHost,
    CRAFT_STAGING_HOST: craftStagingHost,
    REDIS_HOST: `${process.env.REDIS_INSTANCE}-service`
    // SSL: [
    //   {
    //     hosts: [baseDomain, `*.${baseDomain}`],
    //     certFile: process.env.MAIN_CRT,
    //     keyFile: process.env.MAIN_KEY
    //   }
    // ]
  };
  if (process.env.CI_COMMIT_BRANCH === process.env.CI_DEFAULT_BRANCH) {
    return {
      ...common,
      ENVIRONMENT: "production",
      DB_HOST: process.env.DB_HOST,
      DB_PORT: process.env.DB_PORT,
      DB_NAME: process.env.DB_NAME,
      DB_USER: process.env.DB_USER,
      DB_PASSWORD: process.env.DB_PASSWORD,
      NEXT_PRIMARY_HOST: nextProductionHost,
      CRAFT_HOST: craftProductionHost
      // REWRITES: [
      //   {
      //     host: nextProductionHost,
      //     path: "/robots.txt",
      //     rewriteTarget: "/robots.txt",
      //     backendService: "craft"
      //   },
      //   {
      //     host: nextProductionHost,
      //     path: "/sitemap(.*)",
      //     rewriteTarget: "/sitemap$1",
      //     backendService: "craft"
      //   }
      // ]
    };
  }
  if (process.env.CI_COMMIT_BRANCH === "staging") {
    return {
      ...common,
      ENVIRONMENT: "staging",
      // DB_HOST: process.env.GROUP_STAGING_DB_HOST,
      // DB_PORT: process.env.GROUP_STAGING_DB_PORT,
      // DB_NAME: "${PROJECT_SLUG}_staging",
      // DB_USER: process.env.GROUP_STAGING_DB_USER,
      // DB_PASSWORD: process.env.GROUP_STAGING_DB_PASSWORD,
      NEXT_PRIMARY_HOST: nextStagingHost,
      CRAFT_HOST: craftStagingHost
    };
  }
};
