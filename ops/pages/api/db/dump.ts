import { withDevAuth } from "lib/auth";
import { dumpDb } from "lib/db";
import { readStream } from "lib/file";
import { currentTsMilli } from "lib/time";
import * as Rt from "runtypes";

const BodyRuntype = Rt.Record({ dbname: Rt.String });

export default withDevAuth(async (req, res) => {
  const body = BodyRuntype.check(req.body);
  const { dbname } = body;
  const filename = `${dbname}__${currentTsMilli()}.sql`;

  await dumpDb({ database: dbname, filename });

  const stream = readStream(filename);

  stream.pipe(res);
});
