import { withAdminAuth } from "lib/auth";
import { getClient, getClientOps } from "lib/db";
import * as Rt from "runtypes";

const productionDb = process.env.MAIN_DB_NAME as string;
const BodyRuntype = Rt.Record({ copyTo: Rt.String });

export default withAdminAuth(async (req, res) => {
  const body = BodyRuntype.check(req.body);
  const { copyTo } = body;

  if (copyTo === productionDb) {
    throw "you cannot overwrite the production db";
  }

  const client = getClient();
  client.connect();
  const clientOps = getClientOps(client);

  await clientOps.dropDb(copyTo);
  await clientOps.copyDb(productionDb, copyTo);

  client.end();

  res.end(`Success: copied db \`${productionDb}\` to \`${copyTo}\``);
});
