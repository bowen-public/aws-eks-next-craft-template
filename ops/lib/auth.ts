import { NextApiRequest, NextApiResponse } from "next";

const willAuthKey = process.env.WILL_AUTH_KEY as string;
const mattAuthKey = process.env.MATT_AUTH_KEY as string;
const andriyAuthKey = process.env.ANDRIY_AUTH_KEY as string;

export const levels: {
  admin: "admin";
  dev: "dev";
} = {
  admin: "admin",
  dev: "dev"
};

interface People {
  [key: string]: "admin" | "dev" | "testing";
}

const people: People = {
  [willAuthKey]: levels.admin,
  [mattAuthKey]: levels.admin,
  [andriyAuthKey]: levels.dev
};

if (process.env.ENVIRONMENT === "dev") {
  people.testing = "testing";
}

const fail = "Failed: Authentication";

export function adminAuth(req: NextApiRequest): Auth {
  return auth(
    req,
    (key: string) => !!(key && people[key] && people[key] === levels.admin)
  );
}

export function devAuth(req: NextApiRequest): Auth {
  return auth(req, (key: string) => !!(key && people[key]));
}

export function performDevAuth(req: NextApiRequest): void {
  const { success, message } = devAuth(req);

  if (!success) {
    throw message;
  }
}

export function performAdminAuth(req: NextApiRequest): void {
  const { success, message } = adminAuth(req);

  if (!success) {
    throw message;
  }
}

type ApiFunction = (req: NextApiRequest, res: NextApiResponse) => Promise<void>;

export const withDevAuth = (apiFunction: ApiFunction): ApiFunction => {
  return async (req, res) => {
    try {
      performDevAuth(req);

      await apiFunction(req, res);
    } catch (err) {
      if (
        typeof err === "object" &&
        err.code &&
        typeof err.details === "object"
      ) {
        return res.end(`Failed: ${JSON.stringify(err.details)}`);
      }
      if (typeof err === "object" && typeof err.stderr === "string") {
        return res.end(`Failed: ${err.stderr}`);
      }
      if (typeof err === "object") {
        return res.end(`Failed: ${err.toString()}`);
      }
      return res.end(err);
    }
  };
};

export const withAdminAuth = (apiFunction: ApiFunction): ApiFunction => {
  return async (req, res) => {
    try {
      performAdminAuth(req);

      await apiFunction(req, res);
    } catch (err) {
      if (
        typeof err === "object" &&
        err.code &&
        typeof err.details === "object"
      ) {
        return res.end(`Failed: ${JSON.stringify(err.details)}`);
      }
      return res.end(err.stderr ?? err);
    }
  };
};

function auth(req: NextApiRequest, testFn: (key: string) => boolean): Auth {
  if (req.method !== "POST") {
    return { success: false, message: "Failed: Only POST requests allowed" };
  }
  try {
    const key = req.headers.authorization as string;

    if (testFn(key)) {
      return { success: true };
    } else {
      throw fail;
    }
  } catch (err) {
    return { success: false, message: fail };
  }
}

export interface Auth {
  success: boolean;
  message?: string;
}
