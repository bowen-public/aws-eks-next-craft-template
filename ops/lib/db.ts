import { Client, QueryResult } from "pg";
import { exec } from "child_process";
import util from "util";

export const execAsync = util.promisify(exec);

const ssl =
  process.env.ENVIRONMENT === "dev" ? false : { rejectUnauthorized: false };

export const getClient = (database = "postgres"): Client => {
  return new Client({
    user: process.env.MAIN_DB_USER,
    host: process.env.MAIN_DB_HOST,
    database,
    password: process.env.MAIN_DB_PASSWORD,
    port: Number(process.env.MAIN_DB_PORT) || 0,
    ssl
  });
};

export const dumpDb = async ({
  database,
  filename
}: {
  database: string;
  filename: string;
}): Promise<{
  stdout: string;
  stderr: string;
}> => {
  return await execAsync(
    `PGPASSWORD=${process.env.MAIN_DB_PASSWORD} pg_dump -U ${process.env.MAIN_DB_USER} -h ${process.env.MAIN_DB_HOST} -p ${process.env.MAIN_DB_PORT} -d ${database} -O -x > ${filename}`
  );
};

export const getClientOps = (client: Client): ClientOps => ({
  dropDb: async (db: string) => await client.query(dropDb(db)),
  createDb: async (db: string) => await client.query(createDb(db)),
  doesDbExist: async (db: string) => await client.query(doesDbExist(db)),
  copyDb: async (from: string, to: string) =>
    await client.query(copyDb(from, to))
});

export const dropDb = (db: string): string => {
  return `DROP DATABASE IF EXISTS ${db};`;
};

export const createDb = (db: string): string => {
  return `CREATE DATABASE ${db};`;
};

export const doesDbExist = (db: string): string => {
  return `select exists(
    SELECT datname FROM pg_catalog.pg_database WHERE datname = '${db}'
  );`;
};

export const copyDb = (from: string, to: string): string => {
  return `CREATE DATABASE ${to} WITH TEMPLATE ${from};`;
};

type StdOpsFunc = (db: string) => Promise<QueryResult<any>>;
type CopyOpsFunc = (from: string, to: string) => Promise<QueryResult<any>>;

export interface ClientOps {
  dropDb: StdOpsFunc;
  createDb: StdOpsFunc;
  doesDbExist: StdOpsFunc;
  copyDb: CopyOpsFunc;
}
