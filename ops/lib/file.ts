import { createReadStream, promises, ReadStream, unlink } from "fs";
import { gzip } from "node-gzip";

export const asyncReadFile = promises.readFile;

export function readStream(filename: string): ReadStream {
  const stream = createReadStream(filename);

  stream.on("error", function (err) {
    console.error("Read Stream error:", err);
  });

  stream.on("open", function () {
    console.log("Read Stream opened for", filename);
  });

  stream.on("close", function () {
    console.log("Read Stream closed for", filename);
    deleteFile(filename);
  });

  return stream;
}

export function deleteFile(filename: string): void {
  unlink(filename, function (err) {
    if (err) {
      return console.error(err);
    }
    console.log(filename, "deleted");
  });
}

export const gzipFile = async (filename: string): Promise<Buffer> => {
  const fileContents = await asyncReadFile(filename, { encoding: "utf-8" });

  return await gzip(fileContents);
};
