export function currentTsMilli(): number {
  return Date.now();
}

export function currentTs(): number {
  return Math.floor(currentTsMilli() / 1000);
}
