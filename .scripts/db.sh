#!/bin/bash

source .env

DATE=$(date '+%Y.%m.%d__%H.%M.%S')
BACKUP_PATH=".backups/db"

function dump () {
  BACKUP_FILE="${BACKUP_PATH}/${1}__${DATE}.sql"
  curl \
    -H "Authorization: ${BM_OPS_AUTH_KEY}" \
    -H "Content-Type: application/json; charset=UTF-8" \
    -d "{\"dbname\": \"${1}\"}" \
    "${OPS_URL}/api/db/dump" -o ${BACKUP_FILE}
    
  PREVIEW=$(head -c7 ${BACKUP_FILE})

  if [ "$PREVIEW" == "Failed:" ]; then
    printf "\n\n"
    cat "${BACKUP_FILE}"
    printf "\n\n"
    rm "${BACKUP_FILE}"
    exit 1
  fi
}

case $1 in
  copy-prod-to-staging)
    curl \
      -H "Authorization: ${BM_OPS_AUTH_KEY}" \
      -H "Content-Type: application/json; charset=UTF-8" \
      -d "{\"copyTo\": \"${STAGING_DB}\"}" \
      "${OPS_URL}/api/db/copy-production-to"
  ;;
  backup-prod-db)
    dump ${PRODUCTION_DB}
  ;;
  backup-staging-db)
    dump ${STAGING_DB}
  ;;
  migrate-prod-db)
    dump ${PRODUCTION_DB}
    dropdb ${LOCAL_PRODUCTION_DB}
    createdb ${LOCAL_PRODUCTION_DB}
    psql ${LOCAL_PRODUCTION_DB} < ${BACKUP_FILE}
  ;;
  migrate-staging-db)
    dump ${STAGING_DB}
    dropdb ${LOCAL_STAGING_DB}
    createdb ${LOCAL_STAGING_DB}
    psql ${LOCAL_STAGING_DB} < ${BACKUP_FILE}
  ;;
  purge-backups)
    echo "are you sure you want to permanently remove all backups from your local ? (y/n)"
    read ANS
    if [ ${ANS} == "y" ]; then
      rm ${BACKUP_PATH}/*.sql
      exit 0
    fi
  ;;
  *)
    printf "valid arguments: migrate, backup, purge-backups\n"
  ;;
esac
