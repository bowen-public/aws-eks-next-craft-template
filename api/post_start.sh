#!/bin/bash

# since the .env file is copied in at build time on production, we can run these commands here at runtime
cd /usr/share/nginx

./craft install/check

if [ $? == 0 ]; then
  ./craft migrate/all && \
  ./craft project-config/apply && \
  chown -R nginx:nginx ./storage && \
  echo "craft was already installed" > runtime-log.txt
else
  echo "craft was NOT installed yet" > runtime-log.txt
  # waiting for craft to get back to us about how to do this
  # echo $DB_PASSWORD > runtime-log.txt
  # echo./craft setup \
  #   --interactive=false \
  #   --driver=$DB_DRIVER \
  #   --server=$DB_SERVER \
  #   --user=$DB_USER \
  #   --database=$DB_DATABASE \
  #   --port=$DB_PORT \
  #   --password=$DB_PASSWORD \
  #   --schema=$DB_SCHEMA
fi
