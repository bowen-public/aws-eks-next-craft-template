/**
 * Bm module for Craft CMS
 *
 * BmModuleWidget Widget JS
 *
 * @author    wsbm
 * @copyright Copyright (c) 2021 wsbm
 * @link      https://yokohamatire.com
 * @package   BmModule
 * @since     0.0.0
 */

const btnsNodeList = document.querySelectorAll(".bm_ext__btn");
const btnsArr = Array.prototype.slice.call(btnsNodeList);
const Authorization = document.querySelector("#bm_ext__secret").dataset.key;

btnsArr.forEach(function (btn) {
  btn.addEventListener("click", async function () {
    const action = btn.dataset.action;
    const msg = btn.nextElementSibling;

    btn.classList.add("loading");

    try {
      await fetch(action, {
        method: "POST",
        mode: "no-cors"
      });
      msg.innerHTML = "cache cleared";
    } catch (err) {
      msg.innerHTML = "something went wrong";
      console.log(err);
    }

    btn.classList.remove("loading");
    setTimeout(function () {
      msg.innerHTML = "";
    }, 3333);
  });
});
