<?php
/**
 * Bm module for Craft CMS 3.x
 *
 * extension to yokohama craft cms instance
 *
 * @link      https://yokohamatire.com
 * @copyright Copyright (c) 2021 wsbm
 */

namespace modules\bmmodule;

use modules\bmmodule\assetbundles\bmmodule\BmModuleAsset;
use modules\bmmodule\widgets\BmModuleWidget as BmModuleWidgetWidget;

use Craft;
use craft\events\RegisterTemplateRootsEvent;
use craft\events\TemplateEvent;
use craft\i18n\PhpMessageSource;
use craft\web\View;
use craft\web\UrlManager;
use craft\services\Dashboard;
use craft\events\RegisterComponentTypesEvent;
use craft\events\RegisterUrlRulesEvent;

use yii\base\Event;
use yii\base\InvalidConfigException;
use yii\base\Module;

use craft\events\ElementEvent;
use craft\helpers\ElementHelper;
use craft\services\Elements;
use craft\helpers\FileHelper;

/**
 * Craft plugins are very much like little applications in and of themselves. We’ve made
 * it as simple as we can, but the training wheels are off. A little prior knowledge is
 * going to be required to write a plugin.
 *
 * For the purposes of the plugin docs, we’re going to assume that you know PHP and SQL,
 * as well as some semi-advanced concepts like object-oriented programming and PHP namespaces.
 *
 * https://craftcms.com/docs/plugins/introduction
 *
 * @author    wsbm
 * @package   BmModule
 * @since     0.0.0
 *
 */
class BmModule extends Module
{
  // Static Properties
  // =========================================================================

  /**
   * Static property that is an instance of this module class so that it can be accessed via
   * BmModule::$instance
   *
   * @var BmModule
   */
  public static $instance;

  // Public Methods
  // =========================================================================

  /**
   * @inheritdoc
   */
  public function __construct($id, $parent = null, array $config = [])
  {
    Craft::setAlias('@modules/bmmodule', $this->getBasePath());
    $this->controllerNamespace = 'modules\bmmodule\controllers';

    // Translation category
    $i18n = Craft::$app->getI18n();
    /** @noinspection UnSafeIsSetOverArrayInspection */
    if (
      !isset($i18n->translations[$id]) &&
      !isset($i18n->translations[$id . '*'])
    ) {
      $i18n->translations[$id] = [
        'class' => PhpMessageSource::class,
        'sourceLanguage' => 'en-US',
        'basePath' => '@modules/bmmodule/translations',
        'forceTranslation' => true,
        'allowOverrides' => true
      ];
    }

    // Base template directory
    Event::on(View::class, View::EVENT_REGISTER_CP_TEMPLATE_ROOTS, function (
      RegisterTemplateRootsEvent $e
    ) {
      if (
        is_dir(
          $baseDir = $this->getBasePath() . DIRECTORY_SEPARATOR . 'templates'
        )
      ) {
        $e->roots[$this->id] = $baseDir;
      }
    });

    // Set this as the global instance of this module class
    static::setInstance($this);

    parent::__construct($id, $parent, $config);
  }

  /**
   * Set our $instance static property to this class so that it can be accessed via
   * BmModule::$instance
   *
   * Called after the module class is instantiated; do any one-time initialization
   * here such as hooks and events.
   *
   * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
   * you do not need to load it in your init() method.
   *
   */
  public function init()
  {
    parent::init();
    self::$instance = $this;

    // Load our AssetBundle
    if (Craft::$app->getRequest()->getIsCpRequest()) {
      Event::on(View::class, View::EVENT_BEFORE_RENDER_TEMPLATE, function (
        TemplateEvent $event
      ) {
        try {
          Craft::$app->getView()->registerAssetBundle(BmModuleAsset::class);
        } catch (InvalidConfigException $e) {
          Craft::error(
            'Error registering AssetBundle - ' . $e->getMessage(),
            __METHOD__
          );
        }
      });
    }

    // Register our site routes
    Event::on(
      UrlManager::class,
      UrlManager::EVENT_REGISTER_SITE_URL_RULES,
      function (RegisterUrlRulesEvent $event) {
        $event->rules['siteActionTrigger1'] = 'bm-module/default';
      }
    );

    // Register our CP routes
    Event::on(
      UrlManager::class,
      UrlManager::EVENT_REGISTER_CP_URL_RULES,
      function (RegisterUrlRulesEvent $event) {
        $event->rules['cpActionTrigger1'] = 'bm-module/default/do-something';
      }
    );

    // Register our widgets
    Event::on(
      Dashboard::class,
      Dashboard::EVENT_REGISTER_WIDGET_TYPES,
      function (RegisterComponentTypesEvent $event) {
        $event->types[] = BmModuleWidgetWidget::class;
      }
    );

    /**
     * Logging in Craft involves using one of the following methods:
     *
     * Craft::trace(): record a message to trace how a piece of code runs. This is mainly for development use.
     * Craft::info(): record a message that conveys some useful information.
     * Craft::warning(): record a warning message that indicates something unexpected has happened.
     * Craft::error(): record a fatal error that should be investigated as soon as possible.
     *
     * Unless `devMode` is on, only Craft::warning() & Craft::error() will log to `craft/storage/logs/web.log`
     *
     * It's recommended that you pass in the magic constant `__METHOD__` as the second parameter, which sets
     * the category to the method (prefixed with the fully qualified class name) where the constant appears.
     *
     * To enable the Yii debug toolbar, go to your user account in the AdminCP and check the
     * [] Show the debug toolbar on the front end & [] Show the debug toolbar on the Control Panel
     *
     * http://www.yiiframework.com/doc-2.0/guide-runtime-logging.html
     */
    Craft::info(
      Craft::t('bm-module', '{name} module loaded', ['name' => 'Bm']),
      __METHOD__
    );

    Craft::$app->elements->on(Elements::EVENT_AFTER_SAVE_ELEMENT, function (
      ElementEvent $e
    ) {
      if (ElementHelper::isDraftOrRevision($e->element)) {
        return;
      }
      $message = 'test';
      $file = Craft::getAlias('@storage/logs/bmmodule.log');
      $log = date('Y-m-d H:i:s') . ' ' . $message . "\n";

      FileHelper::writeToFile($file, $log, ['append' => true]);
      // Craft::info('HEY', 'application');
      // ...
    });
  }

  // Protected Methods
  // =========================================================================
}
