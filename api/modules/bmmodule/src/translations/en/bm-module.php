<?php
/**
 * Bm module for Craft CMS 3.x
 *
 * extension to yokohama craft cms instance
 *
 * @link      https://yokohamatire.com
 * @copyright Copyright (c) 2021 wsbm
 */

/**
 * Bm en Translation
 *
 * Returns an array with the string to be translated (as passed to `Craft::t('bm-module', '...')`) as
 * the key, and the translation as the value.
 *
 * http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html
 *
 * @author    wsbm
 * @package   BmModule
 * @since     0.0.0
 */
return [
    'Bm plugin loaded' => 'Bm plugin loaded',
];
