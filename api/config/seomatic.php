<?php

use craft\helpers\App;

$config = [];

// if we have the env value for the front end of the site set (MAIN_SITE_URL)
// then configure the siteUrlOverride so we do not need to set it through the dashboard
$mainSiteUrl = App::env('MAIN_SITE_URL');
if ($mainSiteUrl) {
  $config['siteUrlOverride'] = $mainSiteUrl;
}

return $config;
